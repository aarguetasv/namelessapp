CREATE DATABASE IF NOT EXISTS LApp;

USE LApp;

CREATE TABLE IF NOT EXISTS Usuarios(
	IDUsuario int NOT NULL AUTO_INCREMENT,
	Nombre varchar(255) NOT NULL,
	Apellido varchar(255) NOT NULL,
	Edad int NOT NULL,
	Correo varchar(255) NOT NULL,
	Contrasena varchar(255) NOT NULL,
	Nacionalidad varchar(255) NOT NULL,
	PRIMARY KEY (IDUsuario)
);

CREATE TABLE IF NOT EXISTS Areas(
	IDArea int NOT NULL AUTO_INCREMENT,
	Nombre varchar(255) NOT NULL,
	Tipo varchar(10) NOT NULL,
	PRIMARY KEY (IDArea)
);

CREATE TABLE IF NOT EXISTS Transacciones(
	IDTransaccion int NOT NULL AUTO_INCREMENT,
	IDUsuario  int NOT NULL,
	IDArea int NOT NULL,
	Cantidad int NOT NULL,
	Descripcion varchar(255),
	Fecha date NOT NULL,
	PRIMARY KEY (IDTransaccion),
	FOREIGN KEY (IDUsuario) REFERENCES Usuarios(IDUsuario) ON DELETE CASCADE,
	FOREIGN KEY (IDArea) REFERENCES Areas(IDArea) ON DELETE CASCADE
);

CREATE USER 'app'@'%' IDENTIFIED BY '12345';
GRANT DELETE, INSERT, SELECT, UPDATE ON LApp.* TO 'app'@'%';