<?php
include 'conexion.php';
    
     class Transacciones{
         private $pdo;
         public function __construct(){
             try {
                 $this->pdo = Conexion::conectar();
             } catch (Throwable $th) {
                 die($th->getMessage());
             }
         }
 
         public function Registrar($Nombre,$Apellido,$Edad,$correo,$contrasena,$nacionalidad){
             try {
                 $consulta = "INSERT INTO usuarios(Nombre,Apellido,Edad,Correo,Contrasena,Nacionalidad) VALUES(?,?,?,?,?,?)";
                 $this->pdo->prepare($consulta)
                 ->execute(array($Nombre,$Apellido,$Edad,$correo,$contrasena,$nacionalidad));	
             } catch (Throwable $th) {
                 die($th->getMessage());
             }
         }
         public function Usuarios($correo,$contrasena){
            $query = "SELECT * FROM usuarios WHERE Correo = :Correo AND Contrasena = :Contrasena";  
            $statement = $this->pdo->prepare($query);  
            $statement->execute(  
                 array(  
                      'Correo'     =>     $correo,  
                      'Contrasena'     =>     $contrasena  
                 )  
            );
            return $statement;
         }
         public function Areas($tipo = ''){
            if ($tipo == ''){
                $consulta = "SELECT * FROM areas";
            }else {
                $consulta = "SELECT * FROM areas  WHERE Tipo = ?";
            }
            
            $statement = $this->pdo->prepare($consulta);
            $statement->execute(array($tipo));
            return $statement;
          }
          public function transaccion(){
            $consulta = "SELECT * FROM transacciones";
            $statement = $this->pdo->prepare($consulta);
            $statement->execute();
            return $statement;
          }
          public function Agregar($area,$cantidad,$descripcion,$fecha){
            try {
                $consulta = "INSERT INTO transacciones(IDUsuario,IDArea,Cantidad,Descripcion,Fecha) VALUES(?,?,?,?,?)";
                $this->pdo->prepare($consulta)
                ->execute(array($_SESSION["id"],$area,$cantidad,$descripcion,$fecha));
                header("Location: index.php"); //temporal		
            } catch (Throwable $th) {
                die($th->getMessage());
            }
        }
        public function AgregarArea($nombre,$descripcion){
            try {
                $consulta = "INSERT INTO areas(nombre, tipo) VALUES(?,?)";
                $this->pdo->prepare($consulta)
                ->execute(array($nombre,$descripcion));
                header("Location: index.php"); //temporal		
            } catch (Throwable $th) {
                die($th->getMessage());
            }
        }
        public function eliminarArea($IDArea){
            try {
				$consulta = "DELETE FROM areas WHERE IDArea = ?";
                $this->pdo->prepare($consulta)
				->execute(array($IDArea));
                header("Location: areas.php"); //temporal		
            } catch (Throwable $th) {
                die($th->getMessage());
            }
     }
     public function eliminarTransaccion($IDTransaccion){
        try {
            $consulta = "DELETE FROM transacciones WHERE IDTransaccion = ?";
            $this->pdo->prepare($consulta)
            ->execute(array($IDTransaccion));
            header("Location: index.php"); //temporal		
        } catch (Throwable $th) {
            die($th->getMessage());
        }
 }

        public function Grafica($id){
            $consulta1 = "SELECT SUM(transacciones.Cantidad) FROM transacciones INNER JOIN areas ON transacciones.IDArea = areas.IDArea WHERE transacciones.IDUsuario = ? AND areas.Tipo = 'Ingreso';";
            $consulta2 = "SELECT SUM(transacciones.Cantidad) FROM transacciones INNER JOIN areas ON transacciones.IDArea = areas.IDArea WHERE transacciones.IDUsuario = ? AND areas.Tipo = 'Gasto';";
            $stat1 = $this->pdo->prepare($consulta1);
            $stat1->execute(array($id));
            $stat2 = $this->pdo->prepare($consulta2);
            $stat2->execute(array($id));
            $resultado = ["ingresos" => $stat1->fetch(PDO::FETCH_ASSOC)["SUM(transacciones.Cantidad)"],"gastos" => $stat2->fetch(PDO::FETCH_ASSOC)["SUM(transacciones.Cantidad)"]];
            return $resultado;
          }
     }

?>