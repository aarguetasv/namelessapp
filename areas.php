<?php
include_once('header.php');
?>
<body class="w3-black">

<!-- Icon Bar (Sidebar - hidden on small screens) -->
<nav class="w3-top  w3-padding-small w3-small w3-center" id="myNavbar">
  <!-- Avatar image in top left corner -->
  
  <a href="index.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-home w3-xlarge"></i>
    <p>inicio</p>
  </a>
  <a href="ingresos.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-usd w3-xlarge"></i>
    <p>ingresos</p>
  </a>
  <a href="gastos.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-money w3-xlarge"></i>
    <p>gastos</p>
  </a>
  <a href="areas.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-plus-square-o  w3-xlarge"></i>
    <p>areas</p>
  </a>
  <a href="logout.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-times w3-xlarge"></i>
    <p>logout</p>
  </a>
</nav>

<!-- Page Content -->
<div class="w3-padding-large" id="main">
  <!-- Header/Home -->
  <header class="w3-container w3-padding-32 w3-center w3-black" id="home">
  <br>
  <br>
  <?php  
 //login_success.php  
 session_start();  
 include 'Transacciones.php';
 $trans = new Transacciones;
$area = $trans ->Areas('*');
$resultado = $area ->fetchAll(PDO::FETCH_ASSOC);
 if($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (isset($_POST['b1'])) {
     $trans = new Transacciones;
     $trans->AgregarArea($_POST["nombre"],$_POST["tipo"]);
 }
 }
 if (isset($_GET['b2'])) {
     $trans = new Transacciones;
     $trans->EliminarArea($_GET["IDArea"]);
 }
 if(isset($_SESSION["id"]))  
 {  
      echo '<h5>Agregar Area</h5>'; 
 }  
 else  
 {  
      header("location:pdo_login.php");  
 } 
$trans = new Transacciones;
$area = $trans ->Areas();
$resultado = $area ->fetchAll(PDO::FETCH_ASSOC);
 //Extraer todas la filas y almacenarlas en una tabla
$table = "<table border='1' cellpadding='2'>\n";
$table .= "<tr><th>ID</th><th>Nombre</th><th>Tipo</th><th>accion</th></tr>\n";
foreach($resultado as $fila){
$table .= "<tr>
      <td>".$fila["IDArea"]."</td>
      <td>".$fila["Nombre"]."</td>
      <td>".$fila["Tipo"]."</td>
      <td><form method='get' action=''> \n
      <input type='hidden' name='IDArea' value='".$fila["IDArea"]."'>
      <input type='submit' value='Eliminar' name='b2' style=' background-color:red;border: none; color: white;'>
      </form></td>
   </tr>\n";
    }
$table .= "</table>\n"; 
 
 ?>
 <div class="col-12 col-md-6"> 
          <form method="POST" class="w3-container">
            <label>Nombre</label>
            <input type="text" name="nombre" id="descripcion" class="w3-input"></br>
            <br>
			<label>Tipo</label>
      <select class="w3-input" name="tipo" id="tipo" class="w3-select">
              <option value="Ingreso">Ingreso</option>
              <option value="Gasto">Gasto</option>
              </select>
              <br>
			<input type="submit" value="Agregar" class="w3-button w3-white w3-hover-gray" name='b1'>
        </form>
    </div>
	<br>
	<br>
	<?php 

/* Mostrar la tabla con los registros */
echo $table; 

?>
    <!-- Footer -->
  <footer class="w3-content w3-padding-64 w3-text-grey w3-xlarge">

  <!-- End footer -->
  </footer>

<!-- END PAGE CONTENT -->
</div>
  
</body>
</html>