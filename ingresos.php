<?php
include_once('header.php');
?>
<body class="w3-black">
<nav class="w3-top  w3-padding-small w3-small w3-center" id="myNavbar">
  <!-- Avatar image in top left corner -->
  
  <a href="index.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-home w3-xlarge"></i>
    <p>inicio</p>
  </a>
  <a href="ingresos.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-usd w3-xlarge"></i>
    <p>ingresos</p>
  </a>
  <a href="gastos.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-money w3-xlarge"></i>
    <p>gastos</p>
  </a>
  <a href="areas.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-plus-square-o  w3-xlarge"></i>
    <p>areas</p>
  </a>
  <a href="logout.php" class="w3-bar-item w3-button w3-padding-small w3-hover-black">
    <i class="fa fa-times w3-xlarge"></i>
    <p>logout</p>
  </a>
</nav>
<!-- Page Content -->
<div style="margin-top:24px" id="main">
  <!-- Header/Home -->
  <header class="w3-container w3-padding-32 w3-center w3-black" id="home">
  <?php  
 //login_success.php  
 session_start();  

 include 'Transacciones.php';
 
 if(isset($_SESSION["id"]))  
 {  
    echo '<h5>Agregar Ingreso</h5>';
    $trans = new Transacciones;
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $trans->Agregar($_POST["area"],$_POST["cantidad"],$_POST["descripcion"],$_POST["fecha"]);
    }
  }

  else  
  {  
       header("location:pdo_login.php");  
  }
 
 ?>
 <div class="col-12 col-md-6"> 
          <form method="POST" class="w3-container">
            <label>Area</label>
            <select class="w3-input" name="area" id="area" class="w3-select">
<?php

  $trans = new Transacciones;
  $area = $trans->Areas("Ingreso");
  $resultado = $area->fetchAll(PDO::FETCH_ASSOC);

  foreach($resultado as $x){
    echo '<option value="'. $x["IDArea"] . '">' . $x["Nombre"] . '</option>';

  }

?>
            </select>
			<br>
            <label>Descripcion</label>
            <input type="text" name="descripcion" id="descripcion" class="w3-input"></br>
            <br>
			<label>Cantidad</label>
            <input type="number" name="cantidad" id="cantidad" class="w3-input"></br>
            <br>
			<label>Fecha</label>
            <input type="date" name="fecha" id="fecha" class="w3-input"></br>
            <br>
			<input type="submit" value="Agregar" class="w3-button w3-white w3-hover-gray">
        </form>
    </div>
</div>
  
</body>
</html>