<?php  
include_once('header.php');
 session_start();   
 try  
 {  
      include 'Transacciones.php';
      if(isset($_POST["login"]))  
      {  
           if(empty($_POST["Correo"]) || empty($_POST["Contrasena"]))  
           {  
                $message = '<label>All fields are required</label>';  
           }  
           else  
           {
                $tr = new Transacciones;
                $statement = $tr->Usuarios($_POST["Correo"],$_POST["Contrasena"]);
                $count = $statement->rowCount();  
                $row = $statement->fetch(PDO::FETCH_ASSOC);

                if($count > 0)  
                {  
                     $_SESSION["id"] = $row["IDUsuario"];  
                     $_SESSION["nombre"] = $row["Nombre"];
                     header("location:index.php");  
                }  
                else  
                {  
                     $message = '<label>Wrong Data</label>';  
                }  
           }  
      }  
 }  
 catch(PDOException $error)  
 {  
      $message = $error->getMessage();  
 }  
 ?>  
<body class="w3-black">

<!-- Page Content -->
<div class="w3-padding-16" id="main">
  <!-- Header/Home -->
  <header class="w3-container w3-padding-32 w3-center w3-black" id="home">
           <br />  
           <div class="container">  
                <?php  
                if(isset($message))  
                {  
                     echo '<label class="text-danger">'.$message.'</label>';  
                }  
                ?>  
                <h3>Inicio de sesión</h3><br />  
                <form method="post"  class="w3-container">  
                     <label>Correo</label>  
                     <input type="email" name="Correo" class="w3-input" />  
                     <br /> 
					<br>
                     <label>Contraseña</label>  
                     <input type="password" name="Contrasena" class="w3-input" />  
                     <br /> 
<br>	
                     <input type="submit" name="login" class="w3-button w3-white w3-hover-gray" value="Login" /> 
</form>
<br>
<input type="submit" name="login" class="w3-button w3-white w3-hover-gray" onClick="window.location='usuario.php';" value="registrar" />	 
           </div>  
           <br />	   
     </header>
</body>
</html>