<?php
include_once('header.php');
?>
<body class="w3-black">
<!-- Page Content -->
<div class="w3-padding-large" id="main">
  <!-- Header/Home -->
  <header class="w3-container w3-padding-32 w3-center w3-black" id="home">
    <h1 class="w3-xlarge">registro de usuario</h1>
  </header>
  <!-- About Section -->
  <div class="w3-content w3-justify w3-text-grey w3-padding-64" id="about">
<div class="col-12 col-md-6"> 
          <form method="POST" class="w3-container">
            <label>Nombre</label>
            <input type="text" name="Nombre" id="Nombre" class="w3-input" required></br>
			<br>
            <label>Apellido</label>
            <input type="text" name="Apellido" id="Apellido" class="w3-input" required></br>
            <br>
			<label>Edad</label>
            <input type="number" name="Edad" id="Edad" class="w3-input" required></br>
            <br>
			<label>Correo</label>
            <input type="email" name="correo" id="correo" class="w3-input" required></br>
            <br>
			<label>contraseña</label>
            <input type="password" name="contrasena" id="contrasena" class="w3-input" required></br>
            <br>
            <label>contraseña x2</label>
            <input type="password" name="contrasena2" id="contrasena2" class="w3-input" required></br>
            <br>
			<label>Nacionalidad</label>
            <input type="text" name="nacionalidad" id="nacionalidad" class="w3-input" required></br>
            <br>
			<input type="submit" value="registrar" class="w3-button w3-white w3-hover-gray">
			<button onclick="window.location='usuario.php';" class="w3-button w3-white w3-hover-gray">Regresar </button>
        </form>
    </div>
 <?php
    include_once('Transacciones.php');

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        if($_POST["contrasena"] == $_POST["contrasena2"]){
            $reg = new Transacciones;
            $reg->Registrar($_POST['Nombre'],$_POST['Apellido'],$_POST['Edad'],$_POST['correo'],$_POST['contrasena'],$_POST['nacionalidad']);   
            header("Location: index.php");
        }else{
            echo "<h5>las contrasenas no coinciden</h5>";
        }
        	
    }

?>

<!-- END PAGE CONTENT -->
</div>

</body>
</html>